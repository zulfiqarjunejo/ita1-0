'use strict';

import angular from 'angular';
import SettingsController from './settings.controller';

export default angular.module('ita10App.settings', [])
  .controller('SettingsController', SettingsController)
  .name;
