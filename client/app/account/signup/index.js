'use strict';

import angular from 'angular';
import SignupController from './signup.controller';

export default angular.module('ita10App.signup', [])
  .controller('SignupController', SignupController)
  .name;
