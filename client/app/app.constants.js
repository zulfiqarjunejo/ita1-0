'use strict';

import angular from 'angular';

export default angular.module('ita10App.constants', [])
  .constant('appConfig', require('../../server/config/environment/shared'))
  .name;
