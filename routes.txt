Models:
  - User
  - Project
  - Issue
  - Comment

1. User
  > Immediate Requirement
    GET       api/users/                /* Authenticate, and then return list of all users */
    GET       api/users/me              /* Authenticate, and then return current logged in user */
    GET       api/users/:id             /* Authenticate, and return a particular user */
    POST      api/users/                /* Create a new user */
  > Add-on Requirement
    GET       api/users/:id/projects/   /* Authenticate, and return list of projects of particular user */
  > Never Mind Requirement
    PUT       api/users/:id             /* Authenticate, and update a particular user */
    DELETE    api/users/                /* Authenticate, and delete all users */
    DELETE    api/users/:id             /* Authenticate, and delete a particular user */

2. Project
  > Immediate Requirement
    GET       api/projects/             /* Authenticate, and then return list of all projects */
    GET       api/projects/:id          /* Authenticate, and return a particular project */
    POST      api/projects/             /* Authenticate, and create a new project */
    PUT       api/projects/:id          /* Authenticate, and update a particular project */
  > Add-on Requirement
    GET       api/projects/:id/issues   /* Authenticate, and return a list of issues of particular project */
    POST      api/projects/:id/issues   /* Authenticate, and create new issue for particular project */
    DELETE    api/projects/:id/issues   /* Authenticate, and delete all issues of particular project */
  > Never Mind Requirement
    DELETE    api/projects/             /* Authenticate, and delete all projects */
    DELETE    api/projects/:id          /* Authenticate, and delete a particular project */

3. Issue
  > Immediate Requirement
    GET       api/issues/               /* Authenticate, and then return list of all issues */
    GET       api/issues/:id            /* Authenticate, and return a particular issue */
    PUT       api/issues/:id            /* Authenticate, and update a particular issue */
  > Add-on Requirement
    GET       api/issues/:id/comments   /* Authenticate, and return a list of comments of particular issue */
    POST      api/issues/:id/comments   /* Authenticate, and create a new comment for particular issue */
  > Never Mind Requirement
    DELETE    api/issues/               /* Authenticate, and delete all issues */
    DELETE    api/issues/:id            /* Authenticate, and delete a particular issue */
    DELETE    api/issues/:id/comments   /* Authenticate, and delete all comments of particular issue */

4. Comment
  > Immediate Requirement
    GET       api/comments/             /* Authenticate, and then return list of all comments */
    GET       api/comments/:id          /* Authenticate, and return a particular comment */
    PUT       api/comments/:id          /* Authenticate, and update a particular comment */
  > Never Mind Requirement
    DELETE    api/comments/             /* Authenticate, and delete all comments */
    DELETE    api/comments/:id          /* Authenticate, and delete a particular comment */
