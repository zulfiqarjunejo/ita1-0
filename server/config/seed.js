/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from '../api/user/user.model';
import Project from '../api/project/project.model';

User.find({}).remove()
  .then(() => {
    User.create({
      name: 'Test User',
      email: 'test@example.com',
      password: 'test'
    }, {
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    })
    .then(() => {
      console.log('finished populating users');
    });
  });
