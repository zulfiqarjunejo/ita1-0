'use strict';

import Issue from './issue.model';
var issue;
var genIssue = function() {
  issue = new Issue({
    title: 'Fake Issue',
    description: 'Fake description jus to check',
    project: 'project123',
    assignee: 'Assignee User',
    createdAt: Date.now(),
    updatedAt: Date.now(),
    creator: 'Creator User',
  });
  return issue;
};

describe('Issue Model', function() {
  before(function() {
    // Clear users before testing
    return Issue.remove();
  });

  beforeEach(function() {
    genIssue();
  });

  afterEach(function() {
    return Issue.remove();
  });

  it('should begin with no issues', function() {
    return Issue.find({}).exec().should
      .eventually.have.length(0);
  });

  describe('#title', function() {
    it('should fail when saving with a blank title', function() {
      issue.title = '';
      return issue.save().should.be.rejected;
    });

    it('should fail when saving with a null title', function() {
      issue.title = null;
      return issue.save().should.be.rejected;
    });

    it('should fail when saving without a title', function() {
      issue.title = undefined;
      return issue.save().should.be.rejected;
    });
  });

  describe('#project', function() {
    it('should fail when saving with a blank project id', function() {
      issue.project = '';
      return issue.save().should.be.rejected;
    });

    it('should fail when saving with a null project id', function() {
      issue.project = null;
      return issue.save().should.be.rejected;
    });

    it('should fail when saving without project id', function() {
      issue.project = undefined;
      return issue.save().should.be.rejected;
    });
  });
});
