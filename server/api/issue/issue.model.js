'use strict';

import mongoose from 'mongoose';

var IssueSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  project: {
    type: String,
    required: true
  },
  assignee: {
    type: String
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  },
  creator: {
    type: String
  },
  state: {
    type: String
  }
});

IssueSchema
  .path('title')
  .validate(function(title) {
    return title !== '';
  }, 'Title cannot be blank.');

IssueSchema
  .path('project')
  .validate(function(project) {
    return project !== '';
  }, 'Project id cannot be blank.');

export default mongoose.model('Issue', IssueSchema);
