/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/issues              ->  index
 * POST    /api/issues              ->  create
 * GET     /api/issues/:id          ->  show
 * PUT     /api/issues/:id          ->  upsert
 * PATCH   /api/issues/:id          ->  patch
 * DELETE  /api/issues/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Issue from './issue.model';
import Comment from '../comment/comment.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Issues
export function index(req, res) {
  return Issue.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Issue from the DB
export function show(req, res) {
  return Issue.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Issue in the DB
export function create(req, res) {
  var newIssue = new Issue(req.body);
  newIssue.project = req.params.id;
  newIssue.createdAt = Date.now();
  newIssue.updatedAt = Date.now();
  newIssue.creator = req.user._id;
  newIssue.state = 'Active';
  return newIssue.save()
    .then((issue) => {
      res.json({
        info: 'Issue was successfully created.',
        status: true,
        id: issue._id
      });
    })
    .catch(handleError(res));
  // return Issue.create(req.body)
  //   .then(respondWithResult(res, 201))
  //   .catch(handleError(res));
}

// Upserts the given Issue in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Issue.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Issue in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Issue.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Issue from the DB
export function destroy(req, res) {
  return Issue.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

export function getAllByProject(req, res) {
  return Issue.find({project: req.params.id}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function deleteAllByProject(req, res) {

  return Issue.find({project: req.params.id}).exec()
    .then(handleEntityNotFound(res))
    .then((issues) => {
      issues.forEach((issue) => {
        // console.log(issue._id);
        Issue.remove({ _id: issue._id})
          .then((x) => {
            console.log('Issue removed');
            Comment.remove({commentedOn: issue._id})
              .then((y) => {
              })
              .catch(handleError(res));
          })
          .catch(handleError(res));
      });
      res.json({
        info: 'Issue for this project deleted successfully',
        status: true
      });
    })
    .catch(handleError(res));
}
