'use strict';

import Project from './project.model';
var project;
var genProject = function() {
  project = new Project({
    title: 'Fake Project',
    owner: '100x200',
    createdAt: Date.now(),
    updatedAt: Date.now(),
    users: ['200x300', '300x400']
  });
  return project;
};

describe('Project Model', function() {
  before(function() {
    // Clear users before testing
    return Project.remove();
  });

  beforeEach(function() {
    genProject();
  });

  afterEach(function() {
    return Project.remove();
  });

  it('should begin with no projects', function() {
    return Project.find({}).exec().should
      .eventually.have.length(0);
  });

  describe('#title', function() {
    it('should fail when saving with a blank title', function() {
      project.title = '';
      return project.save().should.be.rejected;
    });

    it('should fail when saving with a null title', function() {
      project.title = null;
      return project.save().should.be.rejected;
    });

    it('should fail when saving without a title', function() {
      project.title = undefined;
      return project.save().should.be.rejected;
    });
  });

  describe('#owner', function() {
    it('should fail when saving with a blank owner id', function() {
      project.owner = '';
      return project.save().should.be.rejected;
    });

    it('should fail when saving with a null owner id', function() {
      project.owner = null;
      return project.save().should.be.rejected;
    });

    it('should fail when saving without owner id', function() {
      project.owner = undefined;
      return project.save().should.be.rejected;
    });
  });
});
