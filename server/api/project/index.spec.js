'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var projectCtrlStub = {
  index: 'projectCtrl.index',
  show: 'projectCtrl.show',
  create: 'projectCtrl.create',
  destroy: 'projectCtrl.destroy'
  // ,
  // upsert: 'projectCtrl.upsert',
  // patch: 'projectCtrl.patch',
};

var issueCtrlStub = {
  create: 'issueCtrl.create',
  getAllByProject : 'issueCtrl.getAllByProject',
  deleteAllByProject : 'issueCtrl.deleteAllByProject'
}

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

var authServiceStub = {
  isAuthenticated() {
    return 'authService.isAuthenticated';
  }
};

// require the index with our stubbed out modules
var projectIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './project.controller': projectCtrlStub,
  '../../auth/auth.service': authServiceStub,
  '../issue/issue.controller': issueCtrlStub
});

describe('Project API Router:', function() {
  it('should return an express router instance', function() {
    projectIndex.should.equal(routerStub);
  });

  describe('GET /api/projects', function() {
    it('should route to project.controller.index', function() {
      routerStub.get
        .withArgs('/', 'authService.isAuthenticated', 'projectCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/projects/:id', function() {
    it('should route to project.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'authService.isAuthenticated', 'projectCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/projects', function() {
    it('should route to project.controller.create', function() {
      routerStub.post
        .withArgs('/', 'authService.isAuthenticated', 'projectCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  // describe('PUT /api/projects/:id', function() {
  //   it('should route to project.controller.upsert', function() {
  //     routerStub.put
  //       .withArgs('/:id', 'projectCtrl.upsert')
  //       .should.have.been.calledOnce;
  //   });
  // });

  // describe('PATCH /api/projects/:id', function() {
  //   it('should route to project.controller.patch', function() {
  //     routerStub.patch
  //       .withArgs('/:id', 'projectCtrl.patch')
  //       .should.have.been.calledOnce;
  //   });
  // });

  describe('DELETE /api/projects/:id', function() {
    it('should route to project.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'authService.isAuthenticated', 'projectCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/projects/:id/issues', function() {
    it('should route to issue.controller.getAllByProject', function() {
      routerStub.get
        .withArgs('/:id/issues', 'authService.isAuthenticated', 'issueCtrl.getAllByProject')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/projects/:id/issues', function() {
    it('should route to issue.controller.create', function() {
      routerStub.post
        .withArgs('/:id/issues', 'authService.isAuthenticated', 'issueCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/projects/:id/issues', function() {
    it('should route to issue.controller.deleteAllByProject', function() {
      routerStub.delete
        .withArgs('/:id/issues', 'authService.isAuthenticated', 'issueCtrl.deleteAllByProject')
        .should.have.been.calledOnce;
    });
  });
});
