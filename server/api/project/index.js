'use strict';

var express = require('express');
var controller = require('./project.controller');
var issueController = require('../issue/issue.controller');

import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.post('/', auth.isAuthenticated(), controller.create);

router.get('/:id', auth.isAuthenticated(), controller.show);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

// Never Mind Requirements
// router.put('/:id', controller.upsert);
// router.patch('/:id', controller.patch);

router.get('/:id/issues', auth.isAuthenticated(), issueController.getAllByProject);
router.post('/:id/issues', auth.isAuthenticated(), issueController.create);
router.delete('/:id/issues', auth.isAuthenticated(), issueController.deleteAllByProject);
// router.post('/:id/issues', auth.isAuthenticated(), controller.addIssue);
// router.get('/:id/issues', auth.isAuthenticated(), controller.listIssues);
// router.delete('/:id/issues', auth.isAuthenticated(), controller.deleteIssues);

module.exports = router;
