'use strict';

import mongoose from 'mongoose';

var ProjectSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  owner: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  },
  users: [],
  active: {
    type: Boolean,
    default: true
  }
});

ProjectSchema
  .virtual('summary')
  .get(() => {
    return {
      title: this.title,
      owner: this.owner,
      active: this.active
    }
  });

ProjectSchema
  .path('title')
  .validate(function(title) {
    return title !== ''
  }, 'Title cannot be blank.');

ProjectSchema
  .path('owner')
  .validate((owner) => {
    return owner !== ''
  });

export default mongoose.model('Project', ProjectSchema);
