'use strict';

import Comment from './comment.model';
var comment;
var genComment = function() {
  comment = new Comment({
    content: 'Fake Comment',
    commentedOn: 'issue123',
    postedBy: 'user123',
    createdAt: Date.now(),
    updatedAt: Date.now()
  });
  return comment;
};

describe('Comment Model', function() {
  before(function() {
    // Clear users before testing
    return Comment.remove();
  });

  beforeEach(function() {
    genComment();
  });

  afterEach(function() {
    return Comment.remove();
  });

  it('should begin with no comments', function() {
    return Comment.find({}).exec().should
      .eventually.have.length(0);
  });

  describe('#content', function() {
    it('should fail when saving with a blank content', function() {
      comment.content = '';
      return comment.save().should.be.rejected;
    });

    it('should fail when saving with a null content', function() {
      comment.content = null;
      return comment.save().should.be.rejected;
    });

    it('should fail when saving without content', function() {
      comment.content = undefined;
      return comment.save().should.be.rejected;
    });
  });

  describe('#commentedOn', function() {
    it('should fail when saving with a blank commentedOn', function() {
      comment.commentedOn = '';
      return comment.save().should.be.rejected;
    });

    it('should fail when saving with a null commentedOn', function() {
      comment.commentedOn = null;
      return comment.save().should.be.rejected;
    });

    it('should fail when saving without commentedOn', function() {
      comment.commentedOn = undefined;
      return comment.save().should.be.rejected;
    });
  });
});
