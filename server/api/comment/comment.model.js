'use strict';

import mongoose from 'mongoose';

var CommentSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true
  },
  commentedOn: {
    type: String,
    required: true
  },
  postedBy: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  }
});

CommentSchema
  .path('content')
  .validate(function(content) {
    return content !== '';
  }, 'Content cannot be blank');

CommentSchema
  .path('commentedOn')
  .validate(function(commentedOn) {
    return commentedOn !== '';
  }, 'Issue id cannot be blank');

CommentSchema
  .path('postedBy')
  .validate(function(postedBy) {
    return postedBy !== '';
  }, 'User id cannot be blank');

export default mongoose.model('Comment', CommentSchema);
