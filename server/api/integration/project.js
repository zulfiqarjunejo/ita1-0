'use strict';

var app = require('../..');
import request from 'supertest';

import User from '../user/user.model';
import Project from '../project/project.model';
import Issue from '../issue/issue.model';

var user;
var project;
var issue;

var ownerID;
var projectID;
var issueID;

var token;

var seedUser = function() {
  return User.remove().then(function() {
    user = new User({
      name: 'Fake User',
      email: 'test@example.com',
      password: 'password'
    });

    return user.save().then(function(savedUser) {
      ownerID = savedUser._id;
    });

  });
}

var seedProject = function() {
  return Project.remove().then(function() {
    project = new Project({
      title: 'Fake Project',
      owner: ownerID
    });

    return project.save().then(function(savedProject) {
      projectID = savedProject._id;
    });

  });
}

var seedIssue = function() {
  return Issue.remove().then(function() {
    issue = new Issue({
      title: 'Fake Issue',
      description: 'Fake Description',
      project: projectID
    });

    return issue.save().then(function(savedIssue) {
      issueID = savedIssue._id;
    });

  });
}

describe('Project API:', function() {

  before(function(done) {

    seedUser()
    .then(seedProject)
    .then(seedIssue)
    .then(function() {

      request(app)
        .post('/auth/local')
        .send({
          email: 'test@example.com',
          password: 'password'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          token = res.body.token;
          done();
        });

    }); // Seeding and auth done
  }); // before ends here

  after(function(done) {
    Project.remove(done);
  });

  describe('GET /api/projects', function() {
    var response;

    beforeEach(function(done) {
      request(app)
      .get('/api/projects')
      .set('authorization', 'Bearer ' + token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        response = res.body;
        done();
      });
    });

    it('should respond with JSON array containing 1 project.', function() {
      response.should.be.instanceOf(Array);
      response.length.should.equal(1);
      response[0].title.should.equal('Fake Project');
      response[0].owner.should.equal(ownerID.toString());
    });

  });     // GET /api/projects block ends here

  describe('POST /api/projects', function() {
    var response;

    it('should create a new project IF title is set', function(done) {
      request(app)
      .post('/api/projects')
      .send({title: 'Good Project'})
      .set('authorization', 'Bearer ' + token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        res.body.status.should.equal(true);
        res.body.info.should.equal('Project was successfully added.');
        done();
      });
    });

    it('should not create a new project IF title is not set', function(done) {
      request(app)
      .post('/api/projects')
      .send({description: 'Fake Description'})
      .set('authorization', 'Bearer ' + token)
      .expect(500)
      .end(done);
    });

  });     // POST /api/projects block ends here

  describe('DELETE /api/projects', function() {
    var response;

    it('should delete project IF found', function(done) {
      request(app)
      .post('/api/projects')
      .send({title: 'Good Project'})
      .set('authorization', 'Bearer ' + token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        res.body.status.should.equal(true);
        res.body.info.should.equal('Project was successfully added.');
        request(app)
        .delete('/api/projects/' + res.body.id)
        .set('authorization', 'Bearer ' + token)
        .expect(200)
        .end(done);
      });
    });

    it('should not delete project IF not found, return error', function(done) {
      request(app)
      .delete('/api/projects/')
      .set('authorization', 'Bearer ' + token)
      .expect(404)
      .end(done);
    });

  });     // DELETE /api/projects block ends here

});
