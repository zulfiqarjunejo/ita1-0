'use strict';

// fakeuser
// .get('/api/users/me')
// .set('Authorization', 'bearer ' + token)
// .expect(200)
// .expect('Content-Type', /json/)
// .end((err, res) => {
//   console.log(res.body);
// });

var app = require('../..');
import request from 'supertest';
import Project from './project.model';

var newProject;
var token;

describe('Project API:', function() {
  before(function() {
    return Project.remove();
  });

  var fakeuser = request.agent(app);
  fakeuser
  .post('/api/users')
  .set('Accept', 'application/json')
  .send({name: 'Zulfiqar Junejo', email: 'zulfiqar@hello.com', password: 'hello123'})
  .end(function(err, res){
    token = res.body.token;

    describe('GET /api/projects', function() {

      beforeEach(function() {
        return Project.remove();
      });

      var projects;
      fakeuser
      .get('/api/projects')
      .set('Authorization', 'bearer ' + token)
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        projects = res.body;
      });

       it('should respond with JSON array', function() {
         projects.should.be.instanceOf(Array);
       });

      //  it('should respond with an empty array', function(){
      //    projects.should.deep.equal([]);
      //  });
    });

    describe('POST /api/projects', function() {
      beforeEach(function(done) {
        return Project.remove().then(function() {
          done();
        });
      });

      afterEach(function(){
        return Project.remove();
      });

      var response;
      var id;

      fakeuser
      .post('/api/projects')
      .set('Authorization', 'bearer ' + token)
      .send({title: 'Very Fake Project'})
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        response = res.body;
        id = response._id;
      });

       it('should respond with JSON array', function() {
         response.should.be.instanceOf(Object);
       });

       it('should respond with status true when successfully added', function(){
         response.status.should.equal(true);
       });

       it('should now send the data back with same id', function() {
         fakeuser
         .get('/api/projects')
         .set('Authorization', 'bearer ' + token)
         .expect(200)
         .expect('Content-Type', /json/)
         .end((err, res) => {
           response = res.body;
         });
       });
    });

  });

  // describe('POST /api/projects', function() {
  //   beforeEach(function(done) {
  //     request(app)
  //       .post('/api/projects')
  //       .send({
  //         title: 'New Project',
  //         owner: '100x200'
  //       })
  //       .expect(201)
  //       .expect('Content-Type', /json/)
  //       .end((err, res) => {
  //         if(err) {
  //           return done(err);
  //         }
  //         newProject = res.body;
  //         done();
  //       });
  //   });
  //
  //   it('should respond with the newly created project', function() {
  //     newProject.title.should.equal('New Project');
  //     newProject.owner.should.equal('100x200');
  //   });
  // });

  // describe('GET /api/projects/:id', function() {
  //   var project;
  //
  //   beforeEach(function(done) {
  //     request(app)
  //       .get(`/api/projects/${newProject._id}`)
  //       .expect(200)
  //       .expect('Content-Type', /json/)
  //       .end((err, res) => {
  //         if(err) {
  //           return done(err);
  //         }
  //         project = res.body;
  //         done();
  //       });
  //   });
  //
  //   afterEach(function() {
  //     project = {};
  //   });
  //
  //   it('should respond with the requested project', function() {
  //     project.title.should.equal('New Project');
  //     project.owner.should.equal('100x200');
  //   });
  // });
});
